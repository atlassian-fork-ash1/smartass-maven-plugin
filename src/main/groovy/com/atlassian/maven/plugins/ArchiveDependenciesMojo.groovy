/*
 * Copyright © 2011 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins

import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException

/**
 * Archive dependencies MOJO.
 *
 * @goal archive-dependencies
 */
class ArchiveDependenciesMojo extends DependencyInspectingMojo {

    /**
     * The name of the output file that this MOJO will create.
     *
     * @parameter default-value='${project.build.finalName}.zip'
     */
    String archiveName

    /**
     * Whether to skip the create of the archive
     *
     * @parameter default-value="${skip.smartass.zip.update}"
     */
    boolean skipZipUpdate

    /**
     * The AntBuilder.
     */
    AntBuilder ant = new AntBuilder()

    @Override
    void execute() throws MojoExecutionException, MojoFailureException {
        resolveAllArtifacts { allArtifacts ->

            File archive = new File("${project.build.directory}/${archiveName}")
            long archiveLastModified = archive.exists() ? archive.lastModified() : 0
            if (log.debugEnabled) {
                log.debug "Archive '${archive.absolutePath}' modified on ${new Date(archiveLastModified)}"
            }

            // determine what dependencies need to be updated in the archive
            def updatedArtifacts = allArtifacts.findAll { it.file.lastModified() > archiveLastModified }
            if (log.debugEnabled) {
                allArtifacts.each {
                    log.debug "Artifact ${it.id} resolves to ${it.file.absolutePath} (modified on ${new Date(it.file.lastModified())})"
                }
            }

            // read the cache and compare it with the current set of dependencies
            Set cachedDeps = readDependenciesFromAssFile() as SortedSet
            log.debug "Cached dependency set: ${cachedDeps}"

            def isOverwrite = false
            Set currentDeps = allArtifacts.collect {
                "${it.groupId}:${it.artifactId}:${it.version}".toString()
            } as SortedSet

            if (archive.exists() && currentDeps != cachedDeps) {
                log.info "Dependency set has changed. Rebuilding archive..."
                updatedArtifacts = allArtifacts
                isOverwrite = true
            }

            if (updatedArtifacts && skipZipUpdate) {
                log.info "Skipping the update of $archiveName"
            } else {
                if (updatedArtifacts) {
                    if (archive.exists() && !isOverwrite) {
                        log.info "Updating archive. These artifacts have changed:"
                        updatedArtifacts.each { log.info " - ${it.groupId}:${it.artifactId}:${it.version}" }
                    }

                    // Groovy AntBuilder FTW!
                    archive.parentFile.mkdirs()
                    ant.zip(destfile: archive.absolutePath, update: !isOverwrite) {
                        updatedArtifacts.each { artifact ->
                            zipfileset(file: artifact.file)
                        }
                    }
                } else {
                    log.info "Bundled plugins zip is up to date."
                }

                // write the ass file only if we updated the archive
                writeDependenciesToAssFile(currentDeps)
            }

            // attach the bundled plugins zip
            if (archive.isFile()) {
                projectHelper.attachArtifact(project, "zip", null, archive)
            }
        }
    }

    protected Set readDependenciesFromAssFile() {
        File cacheFile = new File("${project.build.directory}/${archiveName}.ass")

        return cacheFile.exists() ? cacheFile.readLines() : []
    }

    protected void writeDependenciesToAssFile(def dependencies) {
        File cacheFile = new File("${project.build.directory}/${archiveName}.ass")
        cacheFile.parentFile.mkdirs()
        cacheFile.withWriter { writer ->
            dependencies.each { dep ->
                writer.writeLine(dep)
            }
        }
    }
}
