/*
 * Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins

import org.apache.maven.artifact.Artifact
import org.apache.maven.artifact.factory.ArtifactFactory
import org.apache.maven.plugin.MojoExecutionException

class ArtifactUtil {

    private final ArtifactFactory artifactFactory

    ArtifactUtil(ArtifactFactory artifactFactory) {
        this.artifactFactory = artifactFactory
    }

    def toString(Artifact artifact) {
        if (artifact.classifier != null) {
            return "${artifact.groupId}:${artifact.artifactId}:${artifact.version}:${artifact.type}:${artifact.classifier}".toString()
        } else {
            return "${artifact.groupId}:${artifact.artifactId}:${artifact.version}:${artifact.type}".toString()
        }
    }

    /**
     * Decode group:artifact:version:packaging[:classifier]
     */
    def fromString(String gavpAndMaybeC) {
        def components = gavpAndMaybeC.split(':')

        // we can be less strict here, since this intends to read what we produce
        // in generate-artifact-list mojo.
        if (components.length == 5) {
            return artifactFactory.createArtifactWithClassifier(
                    components[0], components[1], components[2], components[3], components[4]
            )
        } else if (components.length == 4) {
            return artifactFactory.createArtifact(
                    components[0], components[1], components[2], null, components[3]
            )
        } else {
            throw new MojoExecutionException("Could not parse artifact string: '${gavpAndMaybeC}'")
        }
    }



}
